// const inventory = require("./inventory");

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const carById = (inventory, id_sent) => {
  if (
    inventory == [] ||
    inventory == "" ||
    inventory == undefined ||
    Object.keys(inventory).length === 0
  )
    return [];
  if (id_sent == [] || id_sent == "" || id_sent == undefined) return [];

  for (let cars of inventory) {
    let { id, car_year, car_model, car_make } = cars;
    if (id_sent == id) {
      console.log(`Car ${id} is a ${car_year} ${car_model} ${car_make}`);
      return [cars];
    }
  }
  return [];
};

module.exports = carById;
