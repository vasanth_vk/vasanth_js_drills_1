// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const carModelsSorted = (inventory) => {
  if (inventory == [] || inventory == "" || inventory == undefined) return [];
  return inventory.sort((a, b) => {
    if (a.car_model.toLowerCase() > b.car_model.toLowerCase()) return 1;
    if (a.car_model.toLowerCase() < b.car_model.toLowerCase()) return -1;
    return 0;
  });
};

module.exports = carModelsSorted;
