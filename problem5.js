// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const carYears = require("./problem4");

const cars2000 = (inventory) => {
  const arrayOfCarYears = carYears(inventory);
  const carsOlderThan2000 = [];
  for (let i = 0; i < arrayOfCarYears.length; i++) {
    if (arrayOfCarYears[i] < 2000) carsOlderThan2000.push(arrayOfCarYears[i]);
  }
  return carsOlderThan2000;
};

module.exports = cars2000;
