// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

const lastCarInInventory = (inventory) => {
  if (inventory == [] || inventory == "" || inventory == undefined) return [];

  let lastCar = inventory[inventory.length - 1];
  if (
    lastCar != undefined &&
    lastCar.hasOwnProperty("car_make") &&
    lastCar.hasOwnProperty("car_model")
  ) {
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
  }
  return [];
};

module.exports = lastCarInInventory;
