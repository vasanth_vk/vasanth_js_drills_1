// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const carYears = (inventory) => {
  if (inventory == [] || inventory == "" || inventory == undefined) return [];
  const arrayOfCarYears = [];
  for (let cars of inventory) {
    const { car_year } = cars;
    arrayOfCarYears.push(car_year);
  }
  return arrayOfCarYears;
};

module.exports = carYears;
